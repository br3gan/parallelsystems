#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mpi.h"

#define TRUE 1
#define FALSE 0
#define DIMENSIONS 2
#define ROWS 0
#define COLUMNS 1
#define MASTER 0

// clockwise direction
#define UP 0
#define RIGHT 1
#define DOWN 2
#define LEFT 3

#define BUFFER 0

int summary_data_grid_dims[DIMENSIONS];
int tasks_number, rank;
int process_grid_dims[DIMENSIONS], process_coords[DIMENSIONS];
int local_data_grid_dims[DIMENSIONS];
float *local_data_grid;
float *recv_buffer;
float *send_buffer;

struct Parms {
  float x;
  float y;
} params = {0.1, 0.1};


/**
* @brief Prints out a percentage loading buffer
*
* @param start The value in which the progress is supposed to start from
* @param end The value in which the progress is going to end to
* @param current The current value of the progress
*/
void loadingBuffer(double start, double end, double current)
{

    double bufferLength = 80.0;

    printf("[");
    int i;
    for (i = 0 ; i < bufferLength ; i++)
    {   
        if ( (int)(current / (end - start) * bufferLength) == i)
        {
	    printf(">");
        }
        else if ( current / (end - start) * bufferLength < i)
        {
	    printf(" ");
        }
        else if ( current / (end - start) * bufferLength > i)
        {
	    printf("=");
        }
    }   
    printf("] %2.1f %% \r", current/(end-start)*100);
}

/**
* @brief Fills the buffer which is going to be sent to left neighbour
*
* @param send_buffer The buffer which will be filled
* @param local_data_grid The grid of the current process
*
* @return 0 upon success
*/
int create_left_send_buffer(float* send_buffer, float* local_data_grid){
    int i;
    for(i=0; i<local_data_grid_dims[ROWS]; i++){
        send_buffer[i]=local_data_grid[i*local_data_grid_dims[COLUMNS]];
    }
    return 0;
}


/**
* @brief Fills the buffer which is going to be sent to right neighbour
*
* @param send_buffer The buffer which will be filled
* @param local_data_grid The grid of the current process
*
* @return 0 upon success
*/
int create_right_send_buffer(float* send_buffer, float* local_data_grid){
    int i;
    for(i=0; i<local_data_grid_dims[ROWS]; i++){
        send_buffer[i+local_data_grid_dims[ROWS]]=
            local_data_grid[i*local_data_grid_dims[COLUMNS]+local_data_grid_dims[COLUMNS]-1];
    }
    return 0;
}

/**
* @brief Calculates the next layer of data
*
* @param read_layer the current read layer
*
* @return 0 upon success
*/
int calculate(char read_layer){

    int row, row_start=0, row_end=local_data_grid_dims[ROWS]-1;
    int column, column_start=0, column_end=local_data_grid_dims[COLUMNS]-1;

    float up_element, down_element, left_element, right_element;

    int write_layer=(1-(read_layer||0))*local_data_grid_dims[ROWS]*local_data_grid_dims[COLUMNS];

    // if the process is on the edge, we don't need to calculate its respective boarder line/column
    if (process_coords[ROWS]==0){
        row_start+=1;
    }
    if (process_coords[ROWS]==(process_grid_dims[ROWS]-1)){
        row_end-=1;
    }
    if (process_coords[COLUMNS]==0){
        column_start+=1;
    }
    if (process_coords[COLUMNS]==(process_grid_dims[COLUMNS]-1)){
        column_end-=1;
    }

    // start from the first row we need to calculate the the last we need to calculate.
    // same for columns
    for (row=row_start; row<=row_end; row++)
        for (column=column_start; column<=column_end; column++){

            // if the first row needs to be calculated, that means we are at a process which has
            // an upper neighbour
            if (row==0){
                up_element=recv_buffer[column];
            }
            else{
                up_element=local_data_grid[read_layer+(row-1)*local_data_grid_dims[COLUMNS]+column];
            }

            // if the last row needs to be calculated, that means we are at a process which has
            // a below neighbour
            if (row==(local_data_grid_dims[ROWS]-1)){
                down_element=recv_buffer[local_data_grid_dims[COLUMNS]+column];
            }
            else{
                down_element=local_data_grid[read_layer+(row+1)*local_data_grid_dims[COLUMNS]+column];
            }

            // if the first column needs to be calculated, that means we are at a process which has
            // a left neighbour
            if (column==0){
                left_element=recv_buffer[2*local_data_grid_dims[COLUMNS]+row];
            }
            else{
                left_element=local_data_grid[read_layer+row*local_data_grid_dims[COLUMNS]+column-1];
            }

            // if the last column needs to be calculated, that means we are at a process which has
            // a right neighbour
            if (column==(local_data_grid_dims[COLUMNS]-1)){
                right_element=recv_buffer[2*local_data_grid_dims[COLUMNS]+local_data_grid_dims[ROWS]+row];
                }
            else{
                right_element=local_data_grid[read_layer+row*local_data_grid_dims[COLUMNS]+column+1];
            }

            // as usual, but we have all the right elements

         local_data_grid[write_layer+row*local_data_grid_dims[COLUMNS]+column]=
	 	local_data_grid[read_layer+row*local_data_grid_dims[COLUMNS]+column]+
                    params.x*(
                        up_element+
                        down_element-
                        2.0*local_data_grid[read_layer+row*local_data_grid_dims[COLUMNS]+column]
                    )
                    +
                    params.y*(
                        left_element+
                        right_element-
                        2.0*local_data_grid[read_layer+row*local_data_grid_dims[COLUMNS]+column]
                    );
	
        }
    }

/**
* @brief Fills the overall grid which contains the data of all processes (not used by default)
*
* @param summary_data_grid Two dimensioned array where the data is going to get copied to.
* @param data_grid_buffer The buffer where the data is going to get copied from.
* @param process_coords[] The coordinates if the process in the process grid.
*
* @return 0 upon success
*/
int fill_summary_grid(float summary_data_grid[][summary_data_grid_dims[COLUMNS]], float *data_grid_buffer, int process_coords[]){

    int row, column;
    for (row=0; row<local_data_grid_dims[ROWS]; row++){
        for (column=0; column<local_data_grid_dims[COLUMNS]; column++){
            summary_data_grid[row+local_data_grid_dims[ROWS]*process_coords[ROWS]][column+local_data_grid_dims[COLUMNS]*process_coords[COLUMNS]]
                =data_grid_buffer[row*local_data_grid_dims[COLUMNS]+column];
        }
    }

    return 0;
}


/**
* @brief Prints out the whole data grid
*
* @param summary_data_grid[][summary_data_grid_dims[COLUMNS]] The whole data grid
*
* @return 0 upon success
*/
int print_summary_grid(float summary_data_grid[][summary_data_grid_dims[COLUMNS]]){

    int row, column;
    for (row=0;row<summary_data_grid_dims[ROWS];row++){
        for (column=0; column<summary_data_grid_dims[COLUMNS];column++){
            printf("%6.1f ", summary_data_grid[row][column]);
        }
        printf("\n");
    }
    printf("\n");
    return 0;
}

int main(int argc, char* argv[]){


    int cart_periodicity[DIMENSIONS], reorder=TRUE;
    int rounds;

    MPI_Comm comm;
 
    cart_periodicity[ROWS]=FALSE;
    cart_periodicity[COLUMNS]=FALSE;

    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD, &tasks_number);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank); 

    if (argc != 6){
        if (rank==MASTER){
            printf("You should provide 5 arguments:\n");
            printf("Px: The vertical dimension of process grid\n");
            printf("Py: The horizontal dimension of process grid\n");
            printf("Dx: The vertical dimension of data grid (multilpy of Px) \n");
            printf("Dy: The horizontal dimension of data grid (multiply of Py)\n");
            printf("N: The number of steps\n");
            printf("Example: mpiexec -n 16 ./MPΙ_2d_heat 4 4 512 512 100\n");
        }
        MPI_Finalize();
        return 2;
    }
    else {
        process_grid_dims[ROWS]=atoi(argv[1]);
        process_grid_dims[COLUMNS]=atoi(argv[2]);
        summary_data_grid_dims[ROWS]=atoi(argv[3]);
        summary_data_grid_dims[COLUMNS]=atoi(argv[4]);
        rounds=atoi(argv[5]);
    }

    // create process grid
    MPI_Cart_create(MPI_COMM_WORLD, DIMENSIONS, process_grid_dims, cart_periodicity, reorder, &comm);
    MPI_Cart_coords(comm, rank, DIMENSIONS, process_coords);

    local_data_grid_dims[ROWS]=summary_data_grid_dims[ROWS]/process_grid_dims[ROWS];
    local_data_grid_dims[COLUMNS]=summary_data_grid_dims[COLUMNS]/process_grid_dims[COLUMNS];

    local_data_grid=malloc(2*local_data_grid_dims[ROWS]*local_data_grid_dims[COLUMNS]*sizeof(float));
    int row, column;

    // if a process is at the edge of the process grid, then zero the cells in the respective positions
    for (row=0; row<local_data_grid_dims[ROWS]; row++){
        for (column=0; column<local_data_grid_dims[COLUMNS]; column++){
            if ((process_coords[ROWS]==0 && row==0) ||
                ((process_coords[ROWS]==(process_grid_dims[ROWS]-1)) && row==(local_data_grid_dims[ROWS]-1)) ||
                (process_coords[COLUMNS]==0 && column==0) ||
                ((process_coords[COLUMNS]==(process_grid_dims[COLUMNS]-1)) && column==(local_data_grid_dims[COLUMNS]-1))
               )
            {
                local_data_grid[row*local_data_grid_dims[COLUMNS]+column]=0;
            }
            else{
                local_data_grid[row*local_data_grid_dims[COLUMNS]+column]=(row+column)*1000;
            }
        }
    }

    memset(local_data_grid+(local_data_grid_dims[ROWS]*local_data_grid_dims[COLUMNS]), 0, local_data_grid_dims[ROWS]*local_data_grid_dims[COLUMNS]*sizeof(float));

    // temporarly save neighbour's coords
    int neighbour[DIMENSIONS];

    int left_neighbour_rank=-1,right_neighbour_rank=-1,up_neighbour_rank=-1,down_neighbour_rank=-1;
    int offset;
    int read_layer, write_layer;

    recv_buffer=malloc((local_data_grid_dims[ROWS]+local_data_grid_dims[COLUMNS])*2*sizeof(float));
    send_buffer=malloc(local_data_grid_dims[ROWS]*2*sizeof(float));

    MPI_Request send_request[4], recv_request[4];
    MPI_Status send_status[4], recv_status[4];

    double start_time = MPI_Wtime();
 
    int round;
    for (round=0; round<rounds; round++){
	
	//Uncomment the following if you want to display the progress bar
	//if (rank == MASTER && round%10==0) loadingBuffer(0.0, rounds*1.0, round*1.0);

        read_layer=(round%2)*local_data_grid_dims[ROWS]*local_data_grid_dims[COLUMNS];
        write_layer=((round+1)%2)*local_data_grid_dims[ROWS]*local_data_grid_dims[COLUMNS];

        // get left neighbour's rank
        if (!(process_coords[COLUMNS]==0)){
            neighbour[ROWS]=process_coords[ROWS];
            neighbour[COLUMNS]=process_coords[COLUMNS]-1;
            MPI_Cart_rank(comm,neighbour,&left_neighbour_rank);
            create_left_send_buffer(send_buffer, read_layer+local_data_grid);
            offset=0;
            MPI_Isend(
                send_buffer+offset, local_data_grid_dims[ROWS], MPI_FLOAT, left_neighbour_rank, 0, comm,
                &send_request[LEFT]
            );
            offset=2*local_data_grid_dims[COLUMNS];
            MPI_Irecv(recv_buffer+offset, local_data_grid_dims[ROWS], MPI_FLOAT, left_neighbour_rank, 0, comm,
                &recv_request[LEFT]
            );
        }
        // get right neighbour's rank
        if (!(process_coords[COLUMNS]==(process_grid_dims[COLUMNS]-1))){
            neighbour[ROWS]=process_coords[ROWS];
            neighbour[COLUMNS]=process_coords[COLUMNS]+1;
            MPI_Cart_rank(comm,neighbour,&right_neighbour_rank);
            create_right_send_buffer(send_buffer, read_layer+local_data_grid);
            offset=local_data_grid_dims[ROWS];
            MPI_Isend(send_buffer+offset, local_data_grid_dims[ROWS], MPI_FLOAT, right_neighbour_rank, 0, comm,
                &send_request[RIGHT]
            );
            offset=2*local_data_grid_dims[COLUMNS]+local_data_grid_dims[ROWS];
            MPI_Irecv(recv_buffer+offset, local_data_grid_dims[ROWS], MPI_FLOAT, right_neighbour_rank, 0, comm,
                &recv_request[RIGHT]
            );
        }
        // get above neighbour's rank
        if (!(process_coords[ROWS]==0)){
            neighbour[ROWS]=process_coords[ROWS]-1;
            neighbour[COLUMNS]=process_coords[COLUMNS];
            MPI_Cart_rank(comm,neighbour,&up_neighbour_rank);
            MPI_Isend(read_layer+local_data_grid, local_data_grid_dims[COLUMNS], MPI_FLOAT, up_neighbour_rank, 0,
                comm, &send_request[UP]
            );
            offset=0;
            MPI_Irecv(recv_buffer+offset, local_data_grid_dims[COLUMNS], MPI_FLOAT, up_neighbour_rank, 0, comm,
                &recv_request[UP]
            );
        }
        // get below neighbour's rank
        if (!(process_coords[ROWS]==(process_grid_dims[ROWS]-1))){
            neighbour[ROWS]=process_coords[ROWS]+1;
            neighbour[COLUMNS]=process_coords[COLUMNS];
            MPI_Cart_rank(comm,neighbour,&down_neighbour_rank);
            MPI_Isend(read_layer+local_data_grid+(local_data_grid_dims[ROWS]-1)*local_data_grid_dims[COLUMNS],
                        local_data_grid_dims[COLUMNS], MPI_FLOAT, down_neighbour_rank, 0, comm,
                        &send_request[DOWN]
            );
            offset=local_data_grid_dims[COLUMNS];
            MPI_Irecv(recv_buffer+offset, local_data_grid_dims[COLUMNS], MPI_FLOAT, down_neighbour_rank, 0,
                comm, &recv_request[DOWN]
            );
        }

        if (left_neighbour_rank!=-1){
            MPI_Wait(&recv_request[LEFT], &recv_status[LEFT]);
        }
        if (right_neighbour_rank!=-1){
            MPI_Wait(&recv_request[RIGHT], &recv_status[RIGHT]);
        }
        if (up_neighbour_rank!=-1){
            MPI_Wait(&recv_request[UP], &recv_status[UP]);
        }
        if (down_neighbour_rank!=-1){
            MPI_Wait(&recv_request[DOWN], &recv_status[DOWN]);
        }

        calculate(read_layer);
    }

    double end_time = MPI_Wtime();
    double total_time = end_time - start_time;
    double total_time_summary;

    MPI_Reduce(&total_time, &total_time_summary, 1, MPI_DOUBLE, MPI_SUM, MASTER, MPI_COMM_WORLD);

    if (rank==MASTER){

	printf("Average time per process: %f\n", total_time_summary/tasks_number);
	// Uncomment the following of you want to print the result
    /*
	// WARINING: STACK OVERFLOW COULD HAPPEN
	float summary_data_grid[summary_data_grid_dims[ROWS]][summary_data_grid_dims[COLUMNS]];

        fill_summary_grid(summary_data_grid, write_layer+local_data_grid, process_coords);

        int process_rank;
        int foreign_process_coords[DIMENSIONS];
        MPI_Status status;
        float *data_grid_buffer=malloc(local_data_grid_dims[ROWS]*local_data_grid_dims[COLUMNS]*sizeof(float));
        for(process_rank=1;process_rank<tasks_number;process_rank++){
            MPI_Cart_coords(comm, process_rank, DIMENSIONS, foreign_process_coords);
            MPI_Recv(data_grid_buffer, local_data_grid_dims[ROWS]*local_data_grid_dims[COLUMNS], MPI_FLOAT, process_rank, 0, comm, &status);
            fill_summary_grid(summary_data_grid, data_grid_buffer, foreign_process_coords);
        }
        //print_summary_grid(summary_data_grid);
        free(data_grid_buffer);
	*/
    }
    /*
    else{
        MPI_Status status;
        MPI_Request request;
        MPI_Isend(write_layer+local_data_grid, local_data_grid_dims[ROWS]*local_data_grid_dims[COLUMNS], MPI_FLOAT, MASTER, 0, comm, &request);
        MPI_Wait(&request, &status);
    }
    */
    free(recv_buffer);
    free(send_buffer);
    free(local_data_grid);

    MPI_Finalize();

    return 0;
}
