#include <stdio.h>
#include <stdlib.h>
#include <time.h>
//library should be at ../common/inc
#include <timestamp.h>


#include <cuda.h>
#include <cuda_runtime_api.h>

#define DIMENSIONS 2
#define ROWS 0
#define COLUMNS 1

#define ROW 0
#define COLUMN 1

__global__ void calculate(float* grid, int x_coord, int y_coord, int threads_per_block, int read_layer, int write_layer){

    struct Parameters{
        float x;
        float y;
    } params = {0.1,0.1};


    int thread_pos = blockIdx.x*blockDim.x+threadIdx.x; 
    int thread_coords[2];
    
    thread_coords[ROW]= thread_pos/x_coord;
    thread_coords[COLUMN]= thread_pos%x_coord;

    if ((thread_coords[ROW]!=0) && thread_coords[ROW]!=(x_coord-1) && 
         thread_coords[COLUMN]!=0 && thread_coords[COLUMN]!=(y_coord-1))
    {
        grid[write_layer+thread_coords[ROW]*y_coord+thread_coords[COLUMN]]=
            grid[read_layer+thread_coords[ROW]*y_coord+thread_coords[COLUMN]]
            +
            params.x*(
                grid[read_layer+(thread_coords[ROW]+1)*y_coord+thread_coords[COLUMN]]+
                grid[read_layer+(thread_coords[ROW]-1)*y_coord+thread_coords[COLUMN]]-
                2.0*grid[read_layer+thread_coords[ROW]*y_coord+thread_coords[COLUMN]]
            )
            +
            params.y*(
                grid[read_layer+thread_coords[ROW]*y_coord+(thread_coords[COLUMN]-1)]+
                grid[read_layer+thread_coords[ROW]*y_coord+(thread_coords[COLUMN]+1)]-
                2.0*grid[read_layer+thread_coords[ROW]*y_coord+thread_coords[COLUMN]]
            );
    }
}

int main(int argc, char* argv[]){

    int grid_dims[DIMENSIONS];
    grid_dims[ROWS]=atoi(argv[1]);
    grid_dims[COLUMNS]=atoi(argv[2]);
    int threads_per_block=atoi(argv[3]);
    int row, column, steps=atoi(argv[4]);
    cudaError errno;

    float* grid=(float*)malloc(2*grid_dims[ROWS]*grid_dims[COLUMNS]*sizeof(float));

    // if a process is at the edge of the process grid, then zero the cells in the respective positions
    for (row=0; row<grid_dims[ROWS]; row++){
        for (column=0; column<grid_dims[COLUMNS]; column++){
            if ( (row==0) || row==(grid_dims[ROWS]-1) || column==0 || column==(grid_dims[COLUMNS]-1) )
            {
                grid[row*grid_dims[COLUMNS]+column]=0;
                grid[(grid_dims[ROWS]*grid_dims[COLUMNS])+row*grid_dims[COLUMNS]+column]=0;
            }
            else{
                grid[row*grid_dims[COLUMNS]+column]=(row+column)*1000;
            }
	    //printf("%3.3f ", grid[row*grid_dims[COLUMNS]+column]);
        }
	//printf("\n");
    }

    float* cuda_grid;
    errno = cudaMalloc((void**)&cuda_grid, 2*grid_dims[ROWS]*grid_dims[COLUMNS]*sizeof(float));
    if ( errno != cudaSuccess ){
        fprintf(stderr, "Cuda:Problem upon allocating memory\n");
        free(grid);
        return -1;
    }
    errno = cudaMemcpy( cuda_grid, grid, 2*grid_dims[ROWS]*grid_dims[COLUMNS]*sizeof(float), cudaMemcpyHostToDevice);
    if ( (errno != cudaSuccess) ) {
        fprintf(stderr, "Cuda:Problem upon copying memory\n");
        free(grid);
        return -1;
    }

    

    dim3 block_dim(threads_per_block);
    dim3 grid_dim((grid_dims[ROWS]*grid_dims[COLUMNS])/threads_per_block);
    int step, read_layer=0, write_layer=grid_dims[ROWS]*grid_dims[COLUMNS];
    
    timestamp start_time = getTimestamp();
    
    for(step=0; step<steps; step++){
        read_layer=(step%2)*grid_dims[ROWS]*grid_dims[COLUMNS];
        write_layer=((step+1)%2)*grid_dims[ROWS]*grid_dims[COLUMNS];
        calculate<<<grid_dim,block_dim>>>(cuda_grid, grid_dims[ROWS], grid_dims[COLUMNS], threads_per_block, read_layer, write_layer);
	cudaThreadSynchronize();
        if ( errno != cudaSuccess ){
            fprintf(stderr, "Cuda:Problem upon synchronization (%s)\n", cudaGetErrorString(errno));
            free(grid);
            return -1;
        } 
    }
    
    double total_time=getElapsedtime(start_time);
    printf("Total time: %f\n", total_time);
    errno=cudaMemcpy( grid, cuda_grid, 2*grid_dims[ROWS]*grid_dims[COLUMNS]*sizeof(float), cudaMemcpyDeviceToHost);
    if ( (errno != cudaSuccess) ) {
        fprintf(stderr, "Cuda:Problem upon copying memory\n");
        free(grid);
        return -1;
    }

//  for(row=0; row<grid_dims[ROWS]; row++){
//    	for(column=0; column<grid_dims[COLUMNS]; column++){
//	    	printf("%3.3f ", grid[write_layer+row*grid_dims[COLUMNS]+column]);
//		}
//		printf("\n");
// 	}

    errno = cudaFree(cuda_grid);
    if ( (errno != cudaSuccess) ) {
        fprintf(stderr, "Cuda:Problem upon deallocating memory\n");
        free(grid);
        return -1;
    }

    free(grid);
    
    return 0;
}
